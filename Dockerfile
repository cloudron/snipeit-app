FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# Configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
COPY apache/snipe-it.conf /etc/apache2/sites-enabled/snipe-it.conf
RUN echo "Listen 3000" > /etc/apache2/ports.conf
RUN a2enmod rewrite headers php8.3

# Configure mod_php
RUN crudini --set /etc/php/8.3/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.save_path /run/snipe-it/sessions && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_divisor 100

RUN cp /etc/php/8.3/apache2/php.ini /etc/php/8.3/cli/php.ini

RUN ln -s /app/data/php.ini /etc/php/8.3/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.3/cli/conf.d/99-cloudron.ini

# renovate: datasource=github-releases depName=snipe/snipe-it versioning=semver extractVersion=^v(?<version>.+)$
ARG SNIPEIT_VERSION=8.0.4

RUN curl -L https://github.com/snipe/snipe-it/archive/v${SNIPEIT_VERSION}.tar.gz | tar -xz --strip-components 1 -f - && \
    ln -sf /app/data/env /app/code/.env && \
    mv /app/code/public/uploads /app/code/public/uploads.orig && ln -sf /app/data/uploads /app/code/public/uploads && \
    chown -R www-data:www-data /app/code

ARG COMPOSER_VERISON=2.2
RUN curl -sS https://getcomposer.org/download/latest-${COMPOSER_VERISON}.x/composer.phar -o /usr/local/bin/composer && chmod +x /usr/local/bin/composer

RUN gosu www-data:www-data composer install --no-dev -o && \
    mv /app/code/storage /app/code/storage.orig && ln -sf /app/data/storage /app/code/storage && \
    gosu www-data:www-data composer clear-cache && \
    rm -rf /app/code/bootstrap/cache && ln -s /run/snipe-it/bootstrap-cache /app/code/bootstrap/cache

# Set up package scripts
COPY start.sh cron.sh env.template /app/pkg/

# Copy in the special CloudronSettings additional artisan task
COPY --chown=www-data:www-data CloudronLdap.php CloudronSetup.php /app/code/app/Console/Commands/

# Lock www-data but allow su - www-data to work
RUN passwd -l www-data && usermod --shell /bin/bash --home /app/data www-data

CMD [ "/app/pkg/start.sh" ]
