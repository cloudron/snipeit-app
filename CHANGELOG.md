[0.1.0]
* Initial version

[0.2.0]
* Update Snipe-IT to 4.9.5

[0.3.0]
* Fix image uploads

[1.0.0]
* Update Snipe-IT to 5.0.4
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.0.0)
* Added attempted logins admin screen
* Added better support for NFC/RFID/Barcode scanners in select boxes
* Added ability to trigger password reset emails for users
* Added Raspbian 9.4 to snipeit.sh installer
* Added department and manager to user import
* Added php version check to upgrade.php
* Added check for minimum PHP version in setup
* Added #5957 - Flysystem support #6262 which allows you to use S3 or Rackspace to host your file uploads
* Added console command to move local files to S3 (or other remote host) for Flysystem support
* Added better asset acceptance flow (thanks, @uberbrady and @tilldeeke!)
* Added console command to send inventory reports to users

[1.0.1]
* Set full_multiple_companies_support to 0 by default

[1.0.2]
* Move sessions to redis

[1.0.3]
* Update Snipe-IT to 5.0.6
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.0.6)
* Added created_at and expires_at to Account API token UI
* Added configured API expiration in years to API token sidebar for clarity
* Fixed #8673 - added category to accessories listing on Account > Assigned Assets

[1.0.4]
* Update Snipe-IT to 5.0.9
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.0.9)
* Added #8781 - asset count by status type in sidenav (#8806)
* Added a new UAC setting to valid User Accounts for Active Directory (#8775)
* Added img-responsive class to preview images for suppliers, etc

[1.0.5]
* Update Snipe-IT to 5.0.10
* Added location to searchableRelations for asset
* Added purchase order and order number to user > licenses view
* Branding page UI improvements (image previews inline)
* Skip posix_getpwuid in upgrader if posix isn’t installed
* Switch backup files array order to show latest first [ch15486]

[1.0.6]
* Update Snipe-IT to 5.0.11
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.0.11)
* Fixed typo on route for licenses
* Fixed #8147 - allow webp image format for public file uploads
* Fixed #8472 (again) - LDAP sync was assigning a bad default location (#8846)
* Only overwrite notes if the LDAP user is new
* Added LDAP flag in users GET API for #8741

[1.0.7]
* Update Snipe-IT to 5.0.12
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.0.12)
* Added an explanation for folks trying to access the API base endpoint with no specific endpoint
* Clarified API url info in account > api
* Localization digit separator feature. (#8915) - Provides an ability to localize the purchase_cost field in front-end hardware index table. Has two digit separator formats in admin settings with comma and dot.

[1.1.0]
* Use latest base image 3.0.0
* Update Snipe-IT to 5.1.1
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.1.0)
* Improved login and forgotten password UI and UX
* Added #9082 - allow deployable status type on checkout
* Added new generic datepicker partial blade which may be useful for additional date fields down the line (c00a1fa)
* Added expected_checkout as editable field in asset edit form (d36d6b8)
* Improvements to checkbox custom field display
* Improvements in contrast and consistency for dark mode skins
* Added Audit date range filter to custom report (#8989)

[1.1.1]
* Update Snipe-IT to 5.1.2
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.1.2)
* Fixed #9129 - added missing function formatDatalistSafe to snipeit_modal.js that prevented ajax select lists from loading
* Fixed #8918 - corrected validation rules on Manufacturer Model where 'name' attribute was incorrect
* Fixed #9145 - color of asset selection in bulk checkout
* Fixed #9115 - Duplicate column name 'provider' in some upgrades/installs
* Fixed weird markdown table formatting in expected checkin report email
* Fixed #9116 - incorrect parameter name sent to hardware.show in expected assets report
* Fixed funky layout in asset model modal window (broken HTML)

[1.1.2]
* Update Snipe-IT to 5.1.3
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.1.3)
* Fixed #9036, #9225, #9085 - corrected ternary that sets the offset to 0 when the offset passed to the API for the user is greater than total locations which was causing pagination to break for location listings. (#9210)
* Fixed a few blade templates for relative URLs, specifically to resolve JS/CSS files not loading for Snipe-IT installations that are in subdirectories (#9170)
* Added a condition to ensure that only assets checked out to a user that is being deleted are updating their status (#9233)
* Fixed links in Models Actions, and to view the fieldset assigned to that Asset Model. (#9232)
* Updated rollbar package
* Fixed #9216 - Deleting users affects locations with the same ID

[1.1.3]
* Update Snipe-IT to 5.1.4
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.1.4)
* Fixed Undefined class constant 'EM_AES_256' in spatie/laravel-backup
* Fixed #9266 - set a colors_array variable even if other conditions are not met
* Fixed #9299: Use correct SVG MIME type for uploads (#9300)
* Fixed custom field validation to include textarea

[1.1.4]
* Update Snipe-IT to 5.1.5
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.1.5)
* Updated languages
* Fixed typo that doesn't accept textarea as custom field type element
* Fixed label name for field_values in custom fields edit
* Increases DPI of barcode for small lables (#9344)
* Extended #6229 to include superuser permission check (#6772)

[1.1.5]
* Update Snipe-IT to 5.1.6
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.1.6)
* Use 24-hour date format for audit
* #9531 - highlight search box when filled, add clear button (#9534)
* #9157 Update .env to API_TOKEN_EXPIRATION_YEARS=15 (reapplies #9524) - this solves an 401 unauthorized issue on 32-bit systems
* #9422 - pivot ID was being used as a user_id (#9512)
* Dockerfile.alpine build error and snipeit runtime permission error (#9520)
* the orderBy clause inside the custom reports function that forms the CSV to be 'id' so making it a unique value and does not cause dupes. [ch14587]
* location printing when relationships are missing/invalid, per #9521

[1.1.6]
* Update Snipe-IT to 5.1.7
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.1.7)
* Fixed an issue when tried to upload a file to an user without actually selecting a file. [ch16471] #9640
* Fixed #9680: Use Eloquent’s withCount() method to count Statuslabel assets
* Fixed #9705 Prevent syntax error in startup.sh
* Allow to bulk update min_amt in Accessory API

[1.1.7]
* Update Snipe-IT to 5.1.8
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.1.8)

[1.2.0]
* Update Snipe-IT to 5.2.0
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.2.0)

[1.3.0]
* Update Snipe-IT to 5.3.0
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.3.0)

[1.3.1]
* Update Snipe-IT to 5.3.1
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.3.1)

[1.3.2]
* Update Snipe-IT to 5.3.2
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.3.2)

[1.3.3]
* Update Snipe-IT to 5.3.3
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.3.3)

[1.3.4]
* Update Snipe-IT to 5.3.5
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.3.5)
* Removed 'actionlog' from the ::with() clause in the asset query API for faster load times on asset listings with many updates
* Fixed an issue where some LDAP translation strings were missing
* Updated all translations from CrowdIn to latest
* Fixed minor XSS vulnerability reported by @laladee

[1.3.5]
* Update base image to 3.1.0

[1.3.6]
* Update Snipe-IT to 5.3.6
* Update base image to 3.2.0

[1.3.7]
* Remove hardcoded memory limit in apache config

[1.3.8]
* Update Snipe-IT to 5.3.7
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.3.7)
* Removed duplicate "department" entry in importer, pulls #10460 to master
* Fixed pagination on Permission Groups page
* Fixed #10469 - increased size of supplier address field
* Removed assets_count on kits query, since it doesn't exist as a column
* Fixed missing index for fieldsets (get in the sea, @uberbrady!)
* Added allow list to modal view options to prevent 500 errors when attempting to access a modal view that doesn't exist
* Adjusted phrasing around auto-incrementing asset tags
* Fixes format property on invalid custom field object
* Fixed access control on BulkAssetModelsController.php

[1.3.9]
* Update Snipe-IT to 5.3.8
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.3.8)

[1.3.10]
* Update Snipe-IT to 5.3.9
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.3.9)
* Added additional gate for selectlists [#10662]
* Fixed issue where bulk edit edits were not showing in Asset's history nor Activity report [sc-16550] [#10586]
* Fixed audit email template formatting
* Fixed LDAP active flag [#10563]
* Fixed "secure hostnames" feature for subdirectory-based Snipe-IT installs
* Temp fix for weird JSON format in history
* Added initial support for default values in checkboxes custom fields
* Fixed /hardware/{id}/checkin API response on error where it was erroneously sending a "success" message on failure
* Added custom date to checkin actionlogs and show it in the history of the asset tab
* Disallow invalid JSON requests via API
* Fixed validation if $model and $model->category exist before return the checkin_email property
* Fixed 500 that would occur if the user who audited an item no longer exists in the system (hard-deleted)

[1.3.11]
* Update Snipe-IT to 5.3.10
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.3.10)

[1.3.12]
* Update Snipe-IT to 5.4.0
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.4.0)

[1.3.13]
* Update Snipe-IT to 5.4.1
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.4.1)
* Fixed an issue where locations dropdown would not load with certain low-level unique permissions
* Fix to better handle metadata display [#10771]
* Fixed edit gate in asset maintenances [#10772]

[1.3.14]
* Update Snipe-IT to 5.4.2
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.4.2)
* Use the max_results env value in the paginator by @snipe in #10797
* Added QR and alt barcode urls to asset transformer by @snipe in #10798
* Added session killer artisan command by @snipe in #10814 [documentation]
* Logout user when their activated status is switched to off by @snipe in #10876

[1.3.15]
* Update Snipe-IT to 5.4.3
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.4.3)
* Backport the license index fix from Develop onto the v5 branch by @uberbrady in https://github.com/snipe/snipe-it/pull/10935
* Ports #10494 to master by @snipe in https://github.com/snipe/snipe-it/pull/10936
* Fixes potential XSS vuln in user requestable results by @snipe in https://github.com/snipe/snipe-it/pull/10942

[1.3.16]
* Update Snipe-IT to 5.4.4
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v5.4.4)
* Added status_id to asset checkout API by @snipe in #10943
* Fixed double updates from action log and history by @Godmartinz in #10952
* Escape checkout target name by @snipe in #10971
* Added access gate to the requested assets index by @snipe in #10991
* Fixed model export filename to use str_slug for filename for JS export (like we do elsewhere) by @snipe in #10994

[1.4.0]
* Update Snipe-IT to 6.0.0
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.0.0)
* SCIM support to allow auto-provisioning of users without LDAP sync
* Logout user when their activated status is switched to off - #10876
* PDF Report of signed EULA + Option to attach files for Download at signature process [#8260]
* Ability to save EULA PDF with signature on asset acceptance
* Ldap_manager field to ldap sync
* Bulk checkin
* PHP8 compatibility
* Restore interface from previous Snipe-IT backup in web UI
* Ability to upload asset images via API
* Components and users count to dashboard
* Dashboard module for locations [ch9199]
* Asset restore to API
* Reqestable as model bulk edit field
* Stricter validation for slack hooks (people were trying to use non-slack urls for webhooks)
* Additional fields to new user modal window [#10421]
* .my.cnf to disable column-statistics backup for Docker
* New concept of "remote worker" and API points to filter by them, as well as option in bulk editing users [#10775]
* First pass at Artisan-based LDAP troubleshooter - [#10033]
* Filter by status_type in StatusLabels API index endpoint [#10829]
* Filter by assets_count, consumables_count, licenses_count, and accessories_count on user API endpoint

[1.4.1]
* Update Snipe-IT to 6.0.1
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.0.1)
* Fixed #11014 - double encoding for custom fields by @snipe in #11065
* Added missing use statement by @snipe in #11070
* Fixed #11052 - add file sizes to upload displays by @snipe in #11073
* Fixes #10706 - Fix saml slo for logout by @johnson-yi in #11076
* Fix route for people file deletion by @snipe in #11077
* Fixed missing backups help string by @snipe in #11079
* Adds encrypted status to custom fields overview by @snipe in #11078
* Removed sr-only class for now - not sure why it’s interfering with the upload button by @snipe in #11080
* Only set manager ID when the lookup succeeds. by @uberbrady in #11088
* Upgraded dompdf by @snipe in #11105
* Fixed bulk-user reset-password links returning 500 by @uberbrady in #11107
* Pass the password along directly instead of retrieving it from the In… by @uberbrady in #11108
* Fixed #11100 for individual users by @snipe in #11111

[1.4.2]
* Update Snipe-IT to 6.0.2
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.0.2)

[1.4.3]
* Update Snipe-IT to 6.0.4
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.0.4)
* Fixed #11304 Trying to access array offset on value of type null at .../Transformers/AssetsTransformer.php by @inietov in #11305

[1.4.4]
* Update Snipe-IT to 6.0.5
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.0.5)
* Fixes Asset location not changing when assigned user's location change via importer by @inietov in #11169
* Fixed SC-19104 - fixes to ldap:troubleshoot artisan command by @uberbrady in #11309
* Fixed #11308 - added bulk edit on statuslabels detail page by @snipe in #11310
* Fixed #11175 - Use the Deja Vu font in PDFs to be able to support Cyrillic, etc by @snipe in #11314
* Nicer suppliers and manufacturers UI by @snipe in #11313
* Tweaked CSS for smaller padlock by @snipe in #11316

[1.4.5]
* Update Snipe-IT to 6.0.6
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.0.6)
* Added missing postal code from user view by @snipe in #11365
* Fixed non-dark mode black theme by @Godmartinz in #11323
* Updated/upgraded JS assets by @snipe in #11367
* Fixed #11378 - Added better excel export by @snipe in #11380
* Fixed check for archived setting on counts by @snipe in #11382
* Fixed missing archived tooltip by @snipe in #11384
* Small fixes to default blue by @snipe in #11386
* Added eula blade for licenses/consumables/components and fixes assigned_to for PDFs by @Godmartinz in #11379
* Fixed dropdown column select font color in bootstrap tables by @Godmartinz in #11375
* Added created_by to users by @snipe in #11383
* Moved the settings search box higher by @snipe in #11387
* Typo of 'general' was in the migration blade a few places by @uberbrady in #11389
* Disallow purge backup deletion by default and, enable via .env by @snipe in #11388
* Upgrade webpack from 5.72.1 to 5.73.0 by @snipe in #11391
* Upgraded guzzle to 7.4.5 by @snipe in #11392
* Adds validation to custom fields' default values by @inietov in #11370
* Fixed #8155 - added disclosure arrows to asset creation/edit by @veenone in #10967
* Remember the state of the disclosure arrows in Assets by @uberbrady in #11403
* Added disclosure arrows for lesser used options on user screen by @snipe in #11407
* Only care about the custom field's converted name when updating the custom field itself by @snipe in #11405
* Fixed custom field transliteration by @snipe in #11404
* Add a new --filter option to Artisan ldap-sync command by @uberbrady in #11408
* Add index across username and deleted_at to improve performance by @uberbrady in #11413
* Fixed #9757 - Added model uploads by @snipe in #11412
* Added additional search filters for location API by @snipe in #11414
* Added additional filters for api indexes by @snipe in #11415
* Added personal access endpoint to API and API token console command by @snipe in #11416
* Added backup download via API by @snipe in #11418
* Fixes BadMethodCallException Call to undefined method App\Models\Asset::unaccepted() for master [ch-17636] by @inietov in #10758
* Fixed #9813: Duplicate function accept-asset by @dampfklon in #9822
* Added checkoutByTag API endpoint for assets by @ntbutler-nbcs in #11417
* Make SCIM be more tolerant of missing fields by @uberbrady in #11430
* Added button to email user list of assets from profile by @Godmartinz in #11427
* Added warnings for common LDAP misconfigs by @snipe in #11435

[1.4.6]
* Update Snipe-IT to 6.0.7
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.0.7)
* Fixed #11393 - reject acceptance if no file is present by @snipe in #11440
* Adds check to AcceptanceController to see if signatures are enabled by @mikeroq #11444
* Fixes #11452 - Asset Restore routes/buttons also asset models/users restore as well by @mikeroq in #11453
* Fixes #11459 Missing import for Setting model in AcceptanceController by @mikeroq in #11460

[1.4.7]
* Update Snipe-IT to 6.0.8
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.0.8)
* Fixed custom fields not populating when creating asset from asset model page by @mikeroq in #11451
* Added covering indexes for performance on license_seats by @uberbrady in #11471
* Upgrade font-awesome to v6 by @snipe in #11478
* Fixes padlock icon by @snipe in #11479
* Changed model file icon by @snipe in #11480
* Fixes license file(s) delete modal showing entire model data instead of just the name by @mikeroq in #11484
* Fixed "print assigned" sig broken by @snipe in #11490
* Fixed migrations on restore by @snipe in #11483
* Bump codacy/codacy-analysis-cli-action from 1.1.0 to 4.1.0 by @dependabot in #11458
* Fixes "email list of all assigned" apparently being successful even if the user has no email address by @mikeroq in #11492
* Added checkbox to exclude archived assets from custom reports by @Godmartinz in #11488
* Fixes #11496 - Wrong URL generated for download of asset acceptance PDF if locale was not en by @mikeroq in #11502
* Fixed #11508 - font size in alert menu by @snipe in #11516
* Changed some autolabeler and code owners by @snipe in #11517
* Use checkbox formatting on BOM and archived checkboxes in custom report by @snipe in #11526

[1.4.8]
* Update Snipe-IT to 6.0.9
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.0.9)
* Upgraded jquery-UI by @snipe in #11531
* Upgraded our fork of laravel-scim-server to better support SCIM creates by @uberbrady in #11528
* Fixed #11540 - accessory api routes from GET to POST by @Godmartinz in #11541
* Added checkin without delete for users by @snipe in #11542
* Fixed #11549 - dark skin dropdown menu fixes by @snipe in #11551
* Upgraded imagemin by @snipe in #11553
* Applies current time to checkin date by @snipe in #11547
* Bumped packages by @snipe in #11554
* Added link to SEDC / perl-www-snipe by @snipe in #11561
* Fixed inconsistent `*_LOG_LEVEL` variables; set reasonable defaults by @uberbrady in #11562
* Added bulk edit to company view by @snipe in #11593
* Switched to conditionally adding the nbsp; that the table needs for proper layout if cell is empty by @snipe in #11578
* Fixed legacy routes for php artisan optimize by @snipe in #11595
* Fixed date format for Current Date output on location print assigned by @snipe in #11597
* Adds options to include Deleted assets to custom asset reports by @Godmartinz in #11594
* Use apropiate string for license actions by @inietov in #11603
* Hide requested assets if the user cannot see requestable items by @snipe in #11604
* Disallow checkout if qty = 0 by @snipe in #11605
* Use unique_undeleted instead of unique for custom fields unique validation by @snipe in #11596
* Error 404 fixed on submitting without mandatory field by @vickyjaura183 in #11601
* Added time diff to EOL in asset view by @snipe in #11608
* Fixed labels on radio buttons in custom report by @snipe in #11609
* Added deleted_at to custom report export by @snipe in #11610
* Added configuration change to maintenance types by @snipe in #11611
* Added relations to report search by @snipe in #11612
* Fix auto incrementing in CSV importer [sc-19366] by @inietov in #11615
* Fixed wrong calculation of assigned components by @inietov in #11617
* Added German (Informal) as a language option (only partially translated currently)

[1.4.9]
* Update Snipe-IT to 6.0.10
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.0.10)
* Added a closing div tag around the mail text help block by @exula in #11635
* Fixed Monthly depreciation calculation if EOL is blank by @Godmartinz in #11642
* Fixed the active flag to be as it was in later v5 releases by @uberbrady in #11648
* Fixed #8928 - add URL and ID to custom asset report by @snipe in #11649
* Fixed #6877 - Added notes to consumables, components on checkout by @snipe in #11650
* Fixes Departments update/store to allow company and/or location ids to be null by @Godmartinz in #11660
* [Snyk] Upgrade acorn from 8.7.1 to 8.8.0 by @snyk-bot in #11664
* Revert "Prevent to delete a user if still has consumables associated to them" by @snipe in #11675
* Added null option for purchase_date and expected_checkin dates by @snipe in #11666
* Fixed #6899 Docker container's php configuration isn't congruent with snipe-it uploads setting size. by @octobunny in #11662
* Added the category in the side info on Models > View by @snipe in #11676
* Fixed #11711 clearfix missing on admin settings page to prevent blanking divs by @Godmartinz in #11712
* Fixed #11706 Manager name by @wewhite in #11707
* Fixed #10565, #11086: correctly looks up Manager DN from AD Manager a… by @wewhite in #11705
* Fixed Translation String: wrong key at request canceled message by @julian-piehl in #11703
* [Snyk] Upgrade webpack from 5.73.0 to 5.74.0 by @snyk-bot in #11681
* [Snyk] Upgrade @fortawesome/fontawesome-free from 6.1.1 to 6.1.2 by @snyk-bot in #11680
* Declare DB_PORT in all .env files by @kylegordon in #11669
* Fixed Search in activity report for full name in relation by @inietov in #11651
* Fixed #11509 allow-ldap-anonymous-bind by @sunflowerbofh in #11510
* Fixed crash on Asset View if file doesn’t exist on server by @snipe in #11723
* Updated compiled assets by @snipe in #11727
* Fixed Validation error when creating custom fields' default values by @inietov in #11724
* Fixed #11709 Dates in custom fields not always stored in the correct format by @inietov in #11726
* Added style to readonly date fields by @snipe in #11728
* Fixed #11708 - pre-create private_storage directories for Docker restores by @uberbrady in #11729
* Fixed #11679 Importing Licenses - Field Mismatch by @inietov in #11730
* Fixed #10593 - added signature column to user history by @snipe in #11731
* Fixed: Log user out of other devices when they change their password by @snipe in #11735

[1.4.10]
* Update Snipe-IT to 6.0.11
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.0.11)
* Fixed 500 error when sending unaccepted assets reminder by @inietov in #11700
* Fixed #11695 Problem with checkin all and delete user. by @inietov in #11738
* Fixed Validation error when empty default customfield values by @inietov in #11737
* Fixed #11742: display correct file sizes when using S3(-like) storage by @dsferruzza in #11752
* Fixed possible XSS on dashboard note by @snipe in #11758
* Fixed the order expiring assets are listed in notifications by @Godmartinz in #11489
* Send notifications when Acceptance Assets actions occur [sc-9917] by @inietov in #11661

[1.4.11]
* Update Snipe-IT to 6.0.12
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.0.12)
* Fixes mobile views and clickability of sidebar menu by @Godmartinz in #11916
* Added autoglossonyms for locales and translations for countries by @snipe in #11924
* Fixed #11710 - Validation of checkbox data with multiple values by @inietov in #11925
* Added start and end dates to users by @snipe in #11926
* Use the new scim-trace feature from our fork of laravel-scim-server lib by @uberbrady in #11927
* Fixed Error 500 undefined variable $item creating a new user. by @inietov in #11933
* Fixes the margin top adjustment for desktop by @Godmartinz in #11937
* Fixed Depreciation date diff readable precision by @inietov in #11959
* Added clearer LDAP activated helptext by @snipe in #11952
* Sets nullable attribute on validation by @snipe in #11960
* Correct comparison logic by @nh314 in #11942
* Remove required mark by @nh314 in #11949
* Bump actions/checkout from 2 to 3.1.0 by @dependabot in #11944
* Use admin ID for audit log notification by @snipe in #11964
* Added new translations by @snipe in #11971

[1.4.12]
* Update Snipe-IT to 6.0.13
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.0.13)
* Added try/catch wrap notifications to catch notification failures on checkin/checkout by @uberbrady in #11834
* Fixes the calc of months and fixes typo of the word depreciation by @Godmartinz in #11986
* Fixed wrong icons for mobile view for locations listing by @snipe in #11988
* Added cache manager lookups in LDAP for performance boost by @uberbrady in #12006
* Added default tab to locations by @snipe in #12008
* Changed button icon for importer by @snipe in #12015
* Added externalId support to SCIM integration by @uberbrady in #12030

[1.4.13]
* Update Snipe-IT to 6.0.14
* Update Cloudron base image to 4.0.0
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.0.14)
* Added new SCIM env vars; and upgraded SCIM library by @uberbrady in #12088
* Fixed 500 on missing license files on server by @snipe in #12105
* Fixed 500 error if model files are missing on server by @snipe in #12107
* Added custom field option to view assets by @snipe in #12096
* Fixed HTTP AssetTransformer to generate an empty object for missing custom fields by @fernando-almeida in #12106
* Allow searching directly for asset tag and serial via main API endpoint by @snipe in #12118
* Replace error suppression with better logic by @snipe in #12121
* Added: API endpoint to trigger a user email notification with their assigned assets by @lukasfehling in #12119
* Added ability to include deleted items in tag search endpoint by @snipe in #12120
* Fixed #12109 - Duplicate column on Category listing by @deloz in #12129
* Fixed 500 if format is not passed to the create custom field endpoint payload by @snipe in #12131
* Fixed issue where View Assigned Assets would crash if there are no custom fields for any assigned models by @snipe in #12132
* Fixed #12095 - email logo still linking even if settings deny it by @deloz in #12127
* Fixed checkout to all description by @Godmartinz in #12122
* Added serial column to the user history page by @Godmartinz in #12141
* Fixed #9949 - PATCH to custom fields failing on validation (alt approach to #12011) by @snipe in #12139
* Fixed restore icon in table view by @whalehub in #12148
* Fixed background color for readonly date picker fields by @snipe in #12157
* Fixed #12046 #12137 Custom field checkboxes not holding value by @inietov in #12165
* Fixed #12161 500 error when accepting assets by @inietov in #12168
* Fixed #12046 #12137 Custom field checkboxes not holding value for develop by @inietov in #12166
* Fixed categories view columns matching by @Robert-Azelis in #12164
* SCIM updates by @uberbrady in #12159
* Fixed #11980 Submit button deactivated in Users' Bulk Checkin if no status selected by @inietov in #12167
* Fixed #11699 and #12065 Email issues by @inietov in #12125
* Added serial number to emailed inventory report by @snipe in #12185
* Fixed #12133 - Added job title to header in print assets view by @snipe in #12187
* Fixed #12119 - updated user inventory language to use strings by @akemidx in #12182
* Fixed life-ring icon color with themes by @Godmartinz in #12194
* Fixed #11682 Custom field type of CUSTOM REGEX defaults back to ANY on editing by @inietov in #12197
* Fixed accidental curly quotes in #12182 by @akemidx in #12204
* Fixed typo in screen reader text by @snipe in #12219
* Fixed regression with the custom report export user fields by @Godmartinz in #12134
* Fixes the margin and padding for img barcode in labels by @Godmartinz in #12224
* Use heroku-redis:mini instead of hobby as It is deprecated by @oguzbilgic in #12231
* Add throttle for password reset form by @snipe in #12221

[1.5.0]
* Update Snipe-IT to 6.1.0
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.1.0)
* We've made a ton of bug fixes and query optimizations in this release, and enhanced many aspects of the UI, including some big improvements to the importer UI (with more on the way.)
* Additionally, we've made lots more progress in replacing hard-coded English strings with translatable strings (much more work on that to come as well.)

[1.5.1]
* Update Snipe-IT to 6.1.1
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.1.1)
* You can now import locations via the importer
* You can now select a supplier for components and consumables
* You can now add cool new dynamic links for warranty urls using placeholder variables that get populated by the asset information
* You can now use more search ranges and multi-selectable values in the Custom Asset Report
* You can now set custom fields to be auto-added to new fieldsets

[1.5.2]
* Update Snipe-IT to 6.1.2
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.1.2)
* Fixed audit log image not appearing by @Godmartinz in #13159
* Moved logic for tighter constraints when ids are passed by @snipe in #13186
* Fixed issue when importing Assets and no status labels exists [sc-23359] by @inietov in #13196
* Set table alias for model number sort scope by @snipe in #13200
* Add missing relationship check in Asset Transformer by @marcusmoore in #13220
* Have ChipperCI run on each PR by @marcusmoore in #13218
* Fix translation string in user importer by @marcusmoore in #13216
* Allow running tests in parallel by @marcusmoore in #13212
* Added half_year fix from @jdickerson71388 by @Godmartinz in #13207

[1.6.0]
* Optional sso support

[1.7.0]
* Update Snipe-IT to 6.2.0
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.2.0)
* We've been trying to fix as many bugs (and sneak in as many small features) as we can before Snipe-IT v7 comes out.

[1.7.1]
* Update Snipe-IT to 6.2.1
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.2.1)
* Added more info in settings by @snipe in #13650
* Fixed bug where checkout to location would throw an error if FMCS was enabled by @snipe in #13655
* Fixed exception being thrown when selected import was deleted by @marcusmoore in #13638
* Fixed #13658 for asset history with encrypted fields by @snipe in #13667
* Fixed #13670 - order number missing from license import by @snipe in #13671
* Fixed #13662 - added clipboard.js by @snipe in #13672
* Fixed bug where license checkout/checkin notes were not being saved by @snipe in #13674
* Set modal focus to modal-name field by @snipe in #13677
* Fieldset Properly Sortable in Asset Models Table by @spencerrlongg in #13678

[1.7.2]
* Update Snipe-IT to 6.2.2
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.2.2)
* Standardize Asset EOL Date by @spencerrlongg in #13585
* Logs non-compliant barcode error as debug message by @marcusmoore in #13694
* Log non-compliant barcode error as debug message by @marcusmoore in #13695
* Fixed potential call to a member function toArray() on null by @marcusmoore in #13696
* [Snyk] Security upgrade css-loader from 4.3.0 to 5.0.0 #13685 by @snipe in #13713

[1.7.3]
* Update Snipe-IT to 6.2.3
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.2.3)
* Added tighter controls for matching log ID and item_id by @snipe in #13726
* Set resend acceptance to POST by @snipe in #13727
* Fixes qty remaining requirements for component checkout via API by @snipe in #13728
* Fixed the storing of group permissions when creating via API by @marcusmoore in #13734
* Fixed notification logic to ensure check in and out emails are delivered by @marcusmoore in #13733

[1.8.0]
* Update Snipe-IT to 6.3.0
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.3.0)

[1.8.1]
* Update Snipe-IT to 6.3.1
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.3.1)

[1.8.2]
* Update Snipe-IT to 6.3.2
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.3.2)
* Allowlist and db prefix for restore by @uberbrady in #14278
* Better handle large downloads on backup API by @snipe in #14296
* Added /backups/latest to API endpoint by @snipe in #14297
* Add missing EXIF PHP library for Alpine Docker image by @uberbrady in #14298
* Check that the model exists before trying to access properties by @snipe in #14299
* Disallow branding uploads in demo mode by @snipe in #14301
* Clean up the file extension on image file uploads by @uberbrady in #14302
* Added ability to bulk delete locations by @snipe in #14304

[1.8.3]
* Update Snipe-IT to 6.3.3
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.3.3)
* Fixed Labels: bulk actions are handled before sorting by @Godmartinz in https://github.com/snipe/snipe-it/pull/14375
* Added route parameter by @snipe in https://github.com/snipe/snipe-it/pull/14372
* Upgrade jspdf-autotable from 3.8.1 to 3.8.2 #14365 by @snipe in https://github.com/snipe/snipe-it/pull/14378
* Fixed 1dbarcodes to populate based on settings by @Godmartinz in https://github.com/snipe/snipe-it/pull/14380
* Added User email check when sending Asset acceptance reminder by @Godmartinz in https://github.com/snipe/snipe-it/pull/14371
* Fixes CVE-2024-27354 and CVE-2024-27355 in phpseclib/phpseclib by @joelpittet in https://github.com/snipe/snipe-it/pull/14370
* Default label setup with custom fields by @Godmartinz in https://github.com/snipe/snipe-it/pull/14320
* Fix #13515: Cannot restore backup by @chandanchowdhury in https://github.com/snipe/snipe-it/pull/14379
* Added console command to encrypt previously unencrypted fields by @snipe in https://github.com/snipe/snipe-it/pull/14385
* Fixed attempting to run bulk actions on an empty asset collection by @marcusmoore in https://github.com/snipe/snipe-it/pull/14388
* Fixed old label engine to work as intended with CSS by @Godmartinz in https://github.com/snipe/snipe-it/pull/14389
* Removed unneeded validation message by @snipe in https://github.com/snipe/snipe-it/pull/14392
* Guard against checking require acceptance on non-existent relationship in accessory model by @marcusmoore in https://github.com/snipe/snipe-it/pull/14393
* [Snyk] Upgrade webpack from 5.90.1 to 5.90.2 by @snipe in https://github.com/snipe/snipe-it/pull/14395
* Ensure Chat and Teams endpoints are not blank before attempting to send webhook on checkout and check in by @marcusmoore in https://github.com/snipe/snipe-it/pull/14394

[1.8.4]
* Update Snipe-IT to 6.3.4
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.3.4)
* Added Somali translation
* Fixed several label setting glitches
* Fixed some API scoping issues where results were not as constrained as they should have been when searching
* Added column selectors and signatures to the "print all assigned" page
* Added 2FA reset logging (this shows up as a separate action, not a normal user edit)
* Added AVIF as an accepted image format
* Added ability to "toggle all" columns in the list views on most pages
* Added the ability to "deep link" to search results in those list views
* Updated the documentation to reflect that the php exif library is required

[1.9.0]
* Update Snipe-IT to 6.4.0
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.4.0)
* Fixed z-index of table load by @Godmartinz in https://github.com/snipe/snipe-it/pull/14520
* Fixed ambiguous id clause in custom report by @snipe in https://github.com/snipe/snipe-it/pull/14551
* Redirect on checkin if the asset is already checked in by @snipe in https://github.com/snipe/snipe-it/pull/14552
* Fixed alignment of dropdown menu for user in nav bar by @Godmartinz in https://github.com/snipe/snipe-it/pull/14547
* Hide/Show encrypted values in hardware list by @mauro-miatello in https://github.com/snipe/snipe-it/pull/14529
* Fixed ldap location syncing incorrect locations for users. by @Godmartinz in https://github.com/snipe/snipe-it/pull/14559
* Added audit dates to label options by @Godmartinz in https://github.com/snipe/snipe-it/pull/14557
* Remove city as required field on location modal by @snipe in https://github.com/snipe/snipe-it/pull/14567
* Reduce Extra Space in Header Dropdown by @akemidx in https://github.com/snipe/snipe-it/pull/14582
* Fixed: Header Dropdown Menus had no hover coloring in dark themes by @akemidx in https://github.com/snipe/snipe-it/pull/14558
* Upgraded Signature-pad.js && Fixed Resizing Canvas on mobile by @Godmartinz in https://github.com/snipe/snipe-it/pull/14577
* Fixed assigned to field in new label engine by @marcusmoore in https://github.com/snipe/snipe-it/pull/14581
* Fixed department validation to allow updates by @Godmartinz in https://github.com/snipe/snipe-it/pull/13880
* Fixed label fields only showing first option by @marcusmoore in https://github.com/snipe/snipe-it/pull/14594
* First fix for user FMCS scoping by @snipe in https://github.com/snipe/snipe-it/pull/14591
* Added test cases around modifying user groups via api by @marcusmoore in https://github.com/snipe/snipe-it/pull/14613
* Feat: add no-interactive flag for upgrade.php by @Q4kK in https://github.com/snipe/snipe-it/pull/14578
* Fixed #14508: Added PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT options to database.php … by @jeffclay in https://github.com/snipe/snipe-it/pull/14511
* Fixed check-all button behaving inconsistently by @snipe in https://github.com/snipe/snipe-it/pull/14622
* Added ico, image/x-icon, image/vnd.microsoft.icon to favicon validation by @snipe in https://github.com/snipe/snipe-it/pull/14628
* Removed escaping on notes for file uploads by @snipe in https://github.com/snipe/snipe-it/pull/14630
* Added "select" option to top of data sources in new label engine by @marcusmoore in https://github.com/snipe/snipe-it/pull/14632
* Fixed dark theme button text coloring by @akemidx in https://github.com/snipe/snipe-it/pull/14616
* Re-enabled updating encrypted custom fields via API [sc-41465] by @uberbrady in https://github.com/snipe/snipe-it/pull/14602
* Left Sidebar Was Not Respecting Theme by @akemidx in https://github.com/snipe/snipe-it/pull/14608
* Removed encrypted fields from label options by @Godmartinz in https://github.com/snipe/snipe-it/pull/14499
* Added a License Export function and button by @Godmartinz in https://github.com/snipe/snipe-it/pull/14587
* Fixed purchase_cost not being allowed to be a string when creating asset via api by @marcusmoore in https://github.com/snipe/snipe-it/pull/14651
* Refactored due/overdue for audit, added due/overdue for checkin API endpoint and GUI by @snipe in https://github.com/snipe/snipe-it/pull/14655
* Added Brother 18mm label type by @snipe in https://github.com/snipe/snipe-it/pull/14391
* Improve RTL support by @mustafa-online in https://github.com/snipe/snipe-it/pull/14679
* Upgrade tecnickcom/tcpdf from version 6.7.4 to 6.7.5 to address the security vulnerability CVE-2024-22640 by @franceslui in https://github.com/snipe/snipe-it/pull/14661
* [Snyk] Upgrade alpinejs from 3.13.5 to 3.13.8 #14646 by @snipe in https://github.com/snipe/snipe-it/pull/14686
* Upgrade fontawesome from 6.5.1 to 6.5.2 by @snipe in https://github.com/snipe/snipe-it/pull/14687
* [Snyk] Upgrade tableexport.jquery.plugin from 1.28.0 to 1.30.0 #14656 by @snipe in https://github.com/snipe/snipe-it/pull/14688
* Capitalize N instead of y since no is default by @KorvinSzanto in https://github.com/snipe/snipe-it/pull/14689

[1.9.1]
* Update Snipe-IT to 6.4.1
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.4.1)
* Load settings in SendUpcomingAuditReport command by @marcusmoore in #14690
* Fixes #14692 - set default variables for sidebar totals by @snipe in #14693
* Fixes #14701 - wrong total asset count by @Toreg87 in #14702
* Fixed UI where delete button was not disabled even if the user couldn't be deleted by @snipe in #14697

[1.9.2]
* Update Snipe-IT to 6.4.2
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v6.4.2)
* Bulk Edit Tests and Tweaks by @spencerrlongg in #14707
* Fixed user assets not updating when a user changes location by @Godmartinz in #14474
* Updated alpine to the latest version (3.13.10) by @marcusmoore in #14700
* Added security.txt file by @snipe in #14725
* Adds a note text area to asset acceptances/declines by @Godmartinz in #14451
* Only attempt to decrypt custom fields in activity log if the value is not empty by @snipe in #14728
* Added next audit date to assets form by @snipe in #14719
* Sets purchase date as date (versus datetime) in labels by @snipe in #14729

[1.10.0]
* Update Snipe-IT to 7.0.3
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v7.0.3)

[1.10.1]
* Update Snipe-IT to 7.0.4
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v7.0.4)

[1.10.2]
* Update Snipe-IT to 7.0.5
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v7.0.5)

[1.10.3]
* Update Snipe-IT to 7.0.6
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v7.0.6)

[1.10.4]
* Update Snipe-IT to 7.0.7
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v7.0.7)

[1.10.5]
* Update Snipe-IT to 7.0.8
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v7.0.8)

[1.10.6]
* Update Snipe-IT to 7.0.9
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v7.0.9)

[1.10.7]
* Update Snipe-IT to 7.0.10
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v7.0.10)

[1.10.8]
* Update Snipe-IT to 7.0.11
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v7.0.11)

[1.10.9]
* Update Snipe-IT to 7.0.12
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v7.0.12)

[1.10.10]
* Update Snipe-IT to 7.0.13
* [Full changelog](https://github.com/snipe/snipe-it/releases/tag/v7.0.13)
* Fixed a couple bugs in CustomFieldSetDefaultValuesForModel component by @marcusmoore in #15486
* Warn user on changing status to undeployable when editing by @snipe in #15492
* Fixed #15504 - allow nulling/not changing locale in user bulk edit by @snipe in #15510
* Fixed misalignment of consumables with smaller media by @Godmartinz in #15493
* Fixes margin for the sidebar menus while using rtl languages by @Godmartinz in #15471
* Fixed check for outside assets on user update validation by @snipe in #15516
* Improved multi-asset create when using numeric prefixes to `asset_tags` by @uberbrady in #15491
* Fixed selected-index of countries drop-down [fd-44144] by @uberbrady in #15521
* Added tests for delete methods in api by @marcusmoore in #15512
* Fixed mail notification field status label not updating by @Godmartinz in #15531

[1.11.0]
* Update snipe-it to 7.1.14
* [Full Changelog](https://github.com/snipe/snipe-it/releases/tag/v7.1.14)
* This is a security release. All Snipe-IT users are strongly encouraged to upgrade.
* Ability to import Asset Models (without accompanying assets) via the Importer
* Ability to override or null out the EOL date for assets via the asset bulk edit screen
* Optimized some queries and indexes to speed things up a bit
* Fixed a bug where OU was accidentally required to create locations via the GUI
* Miscellaneous UI improvements and fixes

[1.11.1]
* Update snipe-it to 7.1.15
* [Full Changelog](https://github.com/snipe/snipe-it/releases/tag/v7.1.15)
* Specify the table name in select for models controller by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/15811
* Fixing [#&#8203;15064](https://github.com/snipe/snipe-it/issues/15064) - to not fail ldap sync on single data issue with ldap  by [@&#8203;maciej-poleszczyk](https://github.com/maciej-poleszczyk) in https://github.com/snipe/snipe-it/pull/15558
* replace the `via()` channel for MS Teams (deprecated) by [@&#8203;Godmartinz](https://github.com/Godmartinz) in https://github.com/snipe/snipe-it/pull/15816

[1.11.2]
* Update snipe-it to 7.1.16
* [Full Changelog](https://github.com/snipe/snipe-it/releases/tag/v7.1.16)
* Fixed Users API `update` from clearing `location_id` unnecessarily by [@&#8203;Godmartinz](https://github.com/Godmartinz) in https://github.com/snipe/snipe-it/pull/15889
* Use transformer for model files by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16101
* Mobile view fix by [@&#8203;Godmartinz](https://github.com/Godmartinz) in https://github.com/snipe/snipe-it/pull/15890
* Hardened asset checkout validation by requiring integer by [@&#8203;marcusmoore](https://github.com/marcusmoore) in https://github.com/snipe/snipe-it/pull/15892
* Fixed [#&#8203;15901](https://github.com/snipe/snipe-it/issues/15901) - re-added required indicator on text and select custom fields by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/15904
* Revert "Hardened asset checkout validation by requiring integer" by [@&#8203;marcusmoore](https://github.com/marcusmoore) in https://github.com/snipe/snipe-it/pull/15908
* Included `MAIL_FROM_ADDR` in phpunit configuration by [@&#8203;marcusmoore](https://github.com/marcusmoore) in https://github.com/snipe/snipe-it/pull/15910

[1.11.3]
* symlink logs to run directory

[1.12.0]
* sync config with upstream

[1.12.1]
* Update snipe-it to 7.1.17
* [Full Changelog](https://github.com/snipe/snipe-it/releases/tag/v7.1.17)
* Fixed potential attempt to divide by zero in transformer by [@&#8203;marcusmoore](https://github.com/marcusmoore) in https://github.com/snipe/snipe-it/pull/16116
* Prevent undefined array key in asset observer by [@&#8203;marcusmoore](https://github.com/marcusmoore) in https://github.com/snipe/snipe-it/pull/16118
* Added Nullsafe  by [@&#8203;spencerrlongg](https://github.com/spencerrlongg) in https://github.com/snipe/snipe-it/pull/16148
* Removed default ternary check in Label view by [@&#8203;Godmartinz](https://github.com/Godmartinz) in https://github.com/snipe/snipe-it/pull/16166
* Added a warning for  webhook `channel_not_found`. by [@&#8203;Godmartinz](https://github.com/Godmartinz) in https://github.com/snipe/snipe-it/pull/16175
* Added ternary check that asset has `asset status` before checking archived by [@&#8203;Godmartinz](https://github.com/Godmartinz) in https://github.com/snipe/snipe-it/pull/16168

[1.13.0]
* Update snipe-it to 8.0.0
* [Full Changelog](https://github.com/snipe/snipe-it/releases/tag/v8.0.0)
* Custom fields on checkin/checkout! (Documentation updates coming shortly)
* Breadcrumbs for better UX
* Better tooltips to explain icons on very tab-dense screens
* You can now add arbitrary notes to assets
* Added Oromo (Ethiopian) as a language
* Added better error handling around rendering barcodes by [@&#8203;marcusmoore](https://github.com/marcusmoore) in https://github.com/snipe/snipe-it/pull/16278
* Added breadcrumbs, route model binding for resource routes by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16272
* Removed php 8.1, added 8.4 by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16290

[1.13.1]
* Update snipe-it to 8.0.1
* [Full Changelog](https://github.com/snipe/snipe-it/releases/tag/v8.0.1)
* Fixed 500 on edit locations page by referencing current location by [@&#8203;jostrander](https://github.com/jostrander) in https://github.com/snipe/snipe-it/pull/16320
* Removed references to mcrypt by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16062
* Fixed edit routes in tests by [@&#8203;marcusmoore](https://github.com/marcusmoore) in https://github.com/snipe/snipe-it/pull/16322
* Changed parameter name for route-model binding by [@&#8203;spencerrlongg](https://github.com/spencerrlongg) in https://github.com/snipe/snipe-it/pull/16321
* Remove remaining hardware references in favor of new RMB by [@&#8203;spencerrlongg](https://github.com/spencerrlongg) in https://github.com/snipe/snipe-it/pull/16325
* Added label test by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16335
* Fixed [#&#8203;16334](https://github.com/snipe/snipe-it/issues/16334) - Changed composer order in upgrade script by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16336
* [@&#8203;jostrander](https://github.com/jostrander) made their first contribution in https://github.com/snipe/snipe-it/pull/16320

[1.13.2]
* Update snipe-it to 8.0.2
* [Full Changelog](https://github.com/snipe)
* Fixed [#&#8203;16173](https://github.com/snipe/snipe-it/issues/16173): `useraccountcontrol` was not included in the ldap query attributes by [@&#8203;joakimbergros](https://github.com/joakimbergros) in https://github.com/snipe/snipe-it/pull/16337
* Fixed [#&#8203;16331](https://github.com/snipe/snipe-it/issues/16331) - Don't make passport:install command require user input by [@&#8203;uberbrady](https://github.com/uberbrady) in https://github.com/snipe/snipe-it/pull/16341
* Fixed renaming custom fields by [@&#8203;marcusmoore](https://github.com/marcusmoore) in https://github.com/snipe/snipe-it/pull/16345
* Nicer `upgrade.php` UI by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16342
* Added `name`, `model_number` and `notes` for stricter asset model API search by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16356
* Added `--no-interaction` to additional passport commands by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16363

[1.13.3]
* Update snipe-it to 8.0.3
* [Full Changelog](https://github.com/snipe)
* Added "Reminder" to subject line of follow up asset checkout emails by [@&#8203;marcusmoore](https://github.com/marcusmoore) in https://github.com/snipe/snipe-it/pull/16156
* Fixed [#&#8203;16371](https://github.com/snipe/snipe-it/issues/16371) - incorrect count and missing name in acceptance reminder email by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16375
* Add some safety around the charset-detection and transliteration in backup restore by [@&#8203;uberbrady](https://github.com/uberbrady) in https://github.com/snipe/snipe-it/pull/16376
* Wrap long text in PDF export in tables by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16378
* Added ldap invert active flag by [@&#8203;azmcnutt](https://github.com/azmcnutt) in https://github.com/snipe/snipe-it/pull/16379
* Avoid using authenticated user's email address in email partial by [@&#8203;marcusmoore](https://github.com/marcusmoore) in https://github.com/snipe/snipe-it/pull/16398
* Adds Translation strings to General and Branding Settings by [@&#8203;Godmartinz](https://github.com/Godmartinz) in https://github.com/snipe/snipe-it/pull/16150
* Fixed duplicate entries preventing LDAP sync from continuing by [@&#8203;Fiala06](https://github.com/Fiala06) in https://github.com/snipe/snipe-it/pull/15911
* Refactored audit notification to mail, added test, added alerts check to scheduler by [@&#8203;Godmartinz](https://github.com/Godmartinz) in https://github.com/snipe/snipe-it/pull/16251
* Fixed [#&#8203;16402](https://github.com/snipe/snipe-it/issues/16402) - localize "each" string in components tab on asset view by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16403
* Fixed [#&#8203;16386](https://github.com/snipe/snipe-it/issues/16386) - some fields not populating with user data on edit by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16409
* Fixed [#&#8203;16407](https://github.com/snipe/snipe-it/issues/16407) - weird layout on components for non super user by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16410
* Fixed regression from [#&#8203;16150](https://github.com/snipe/snipe-it/issues/16150) where branding page could not be saved by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16412
* Nicer model name formatting on RMB model not found by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16415
* [@&#8203;azmcnutt](https://github.com/azmcnutt) made their first contribution in https://github.com/snipe/snipe-it/pull/16379
* [@&#8203;Fiala06](https://github.com/Fiala06) made their first contribution in https://github.com/snipe/snipe-it/pull/15911

[1.14.0]
* Update base image to 5.0.0

[1.14.1]
* Update snipe-it to 8.0.4
* [Full Changelog](https://github.com/snipe)
* Adds audit notification for MS Teams by [@&#8203;Godmartinz](https://github.com/Godmartinz) in https://github.com/snipe/snipe-it/pull/16414
* Changed `visited-link` and `link` colors in default theme to be more accessible by [@&#8203;Godmartinz](https://github.com/Godmartinz) in https://github.com/snipe/snipe-it/pull/16413
* Fixed custom report template route for installations in subdirectories by [@&#8203;marcusmoore](https://github.com/marcusmoore) in https://github.com/snipe/snipe-it/pull/16420
* Fixed user create modal - check if `$item` is set by [@&#8203;snipe](https://github.com/snipe) in https://github.com/snipe/snipe-it/pull/16427

