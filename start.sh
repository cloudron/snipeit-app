#!/bin/bash

set -eu

mkdir -p /run/snipe-it/{logs,sessions,cache} /app/data/uploads /app/data/storage /run/snipe-it/bootstrap-cache

if [[ ! -f /app/data/env ]]; then
    echo "==> Copying default configuration"
    cp /app/pkg/env.template /app/data/env

    cp -r /app/code/storage.orig/* /app/data/storage
    cp -r /app/code/public/uploads.orig/* /app/data/uploads
else
    crudini --set /app/data/env "" SESSION_DRIVER redis
    crudini --set /app/data/env "" APP_FORCE_TLS true
fi

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

[[ -z "${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-}" ]] && export CLOUDRON_MAIL_FROM_DISPLAY_NAME="Snipe-IT"

if [[ ! -f /app/data/.setup-complete ]]; then
    echo "==> Running first-time setup"

    # Generate the encryption key
    php /app/code/artisan key:generate --force

    sed -e "s/ENCRYPT=.*/ENCRYPT=true/" -i /app/data/env

    # Install the migrations table and run all migrations
    php /app/code/artisan migrate:install
    php /app/code/artisan migrate --force
    php /app/code/artisan snipeit:create-admin --first_name "Snipe-IT" --last_name "Admin" --email="admin@cloudron.local" --username "admin" --password "changeme123"
 
    # Set default settings in the database
    php /app/code/artisan snipeit:cloudron-setup

    touch /app/data/.setup-complete
fi

echo "=> Migrating database"
php /app/code/artisan migrate --force

if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
    echo "=> Configuring Cloudron LDAP"
    php /app/code/artisan snipeit:cloudron-ldap
fi

# sessions directory
rm -rf /app/data/storage/framework/sessions && ln -s /run/snipe-it/sessions /app/data/storage/framework/sessions
rm -rf /app/data/storage/framework/cache && ln -s /run/snipe-it/cache /app/data/storage/framework/cache
rm -rf /app/data/storage/logs && ln -s /run/snipe-it/logs /app/data/storage/logs

# https://snipe-it.readme.io/docs/common-issues (clear caches after update)
echo "=> Clearing caches"
php artisan view:clear
php artisan cache:clear
php artisan route:clear

echo "=> Changing permissions"
chown -R www-data:www-data /app/data /run/snipe-it

APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
