<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\Models\Setting;

class CloudronSetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'snipeit:cloudron-setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will set the basic Snipe-IT settings to Cloudron first-time setup defaults. ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Setting Snipe-IT Cloudron first-time setup defaults');
        $settings = new Setting;
        // https://snipe-it.readme.io/docs/general-settings#full-multiple-companies-support. this creates a multi-tenant setup
        // It can cause confusion for people who don’t realize that allocating assets, etc to a company with that turned on means some people will not be able to see those assets
        $settings->full_multiple_companies_support = 0;
        $settings->site_name = 'Snipe-IT Asset Management';
        $settings->alert_email = env('CLOUDRON_MAIL_FROM');
        $settings->alerts_enabled = 1;
        $settings->pwd_secure_min = 10;
        $settings->brand = 1;
        $settings->locale = 'em';
        $settings->default_currency = 'USD';
        $settings->created_by = 1;
        $settings->email_domain = env('CLOUDRON_MAIL_DOMAIN');
        $settings->email_format = 'firstname.lastname';
        $settings->next_auto_tag_base = 1;
        $settings->auto_increment_assets = 0;
        $settings->save();
    }

}
