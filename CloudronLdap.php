<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\Models\Setting;
use \Illuminate\Encryption\Encrypter;

class CloudronLdap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'snipeit:cloudron-ldap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will set the Snipe-IT LDAP settings to Cloudron-integrated defaults. ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Setting Cloudron LDAP integration settings');
        $encrypter = new Encrypter(base64_decode(substr(config('app.key'), 7)), config('app.cipher'));

        $settings = Setting::first();
        $settings->custom_forgot_pass_url = env('CLOUDRON_API_ORIGIN') . '/login.html?passwordReset';
        $settings->ldap_enabled = 1;
        $settings->ldap_server = env('CLOUDRON_LDAP_URL');
        $settings->ldap_uname = env('CLOUDRON_LDAP_BIND_DN');
        $settings->ldap_pword = $encrypter->encrypt(env('CLOUDRON_LDAP_BIND_PASSWORD'));
        $settings->ldap_basedn = env('CLOUDRON_LDAP_USERS_BASE_DN');
        $settings->ldap_filter = '&(objectclass=user)';
        $settings->ldap_username_field = 'username';
        $settings->ldap_lname_field = 'sn';
        $settings->ldap_fname_field = 'givenname';
        $settings->ldap_auth_filter_query = 'username=';
        $settings->ldap_email = 'mail';
        $settings->ldap_pw_sync = 0;
        $settings->save();
    }

}
