#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Snipe-IT', function () {
    this.timeout(0);

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    let browser, app;

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 100000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    });

    after(function () {
        browser.quit();
    });

    async function saveScreenshot(fileName) {
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${fileName.replaceAll(' ', '_')}-${new Date().getTime()}.png`, screenshotData, 'base64');
    }

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');
        await saveScreenshot(this.currentTest.title);
    });

    async function waitForElement (elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT)
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo () {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(a => a.location === LOCATION || a.location === LOCATION + '2')[0];
        expect(app).to.be.an('object');
    }

    async function adminLogin () {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/login');

        await waitForElement(By.id('username'));
        await browser.findElement(By.id('username')).sendKeys('admin');
        await browser.findElement(By.id('password')).sendKeys('changeme123');
        await browser.findElement(By.xpath('//button[text()="Login"]')).click();

        // 500 error workaround
        await browser.sleep(2000);
        await browser.get('https://' + app.fqdn);

        await waitForElement(By.xpath('//h2[contains(text(), "This is your dashboard")]'));
    }

    async function ldapLogin (username, password) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/login');

        await waitForElement(By.id('username'));
        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Login"]')).click();

        // 500 error workaround
        await browser.sleep(2000);
        await browser.get('https://' + app.fqdn);

        await waitForElement(By.xpath('//a[contains(@href, "#asset")]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);

        await waitForElement(By.xpath('//a[@class="dropdown-toggle"]/img'));
        await browser.findElement(By.xpath('//a[@class="dropdown-toggle"]/img')).click();
        await waitForElement(By.xpath('//a[contains(@href, "/logout")]'));
        await browser.findElement(By.xpath('//a[contains(@href, "/logout")]')).click();
        await waitForElement(By.id('username'));
    }

    async function addStatusLabel() {
        await browser.get('https://' + app.fqdn + '/statuslabels/create');

        await waitForElement(By.id('name'));
        await browser.findElement(By.id('name')).sendKeys('Lost');
        await browser.findElement(By.xpath('//button[contains(., "Save")]')).click();
        await browser.sleep(2000);

        await waitForElement(By.xpath('//a[contains(., "Lost")]'));
    }

    async function checkStatusLabel() {
        await browser.get('https://' + app.fqdn + '/statuslabels');

        await waitForElement(By.xpath('//a[contains(., "Lost")]'));
    }

    it('install app',  function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', adminLogin);
    it('can add status label', addStatusLabel);
    it('can logout', logout);
    it('can login', ldapLogin.bind(null, username, password));
    it('can logout', logout);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id, EXEC_ARGS); });

    it('can login', adminLogin);
    it('can check status label', checkStatusLabel);
    it('can logout', logout);

    it('can login', ldapLogin.bind(null, username, password));
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', adminLogin);
    it('can check status label', checkStatusLabel);
    it('can logout', logout);

    it('can login', ldapLogin.bind(null, username, password));
    it('can logout', logout);

    it('move to different location', function () { execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS); });
    it('can get new app information', getAppInfo);

    it('can login', adminLogin);
    it('can check status label', checkStatusLabel);
    it('can logout', logout);

    it('can login', ldapLogin.bind(null, username, password));
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    it('install app (no sso)',  function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', adminLogin);
    it('can add status label', addStatusLabel);
    it('can logout', logout);
    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test updates
    it('can install app', function () { execSync(`cloudron install --appstore-id ${app.manifest.id} --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', adminLogin);
    it('can add status label', addStatusLabel);
    it('can logout', logout);

    it('can update', function () { execSync('cloudron update --app ' + app.id, EXEC_ARGS); });

    it('can login', adminLogin);
    it('can check status label', checkStatusLabel);
    it('can logout', logout);
    it('can login', ldapLogin.bind(null, username, password));
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});

